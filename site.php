<?php
/**
 * genereer tabel met cursusdata
 *
 * @return string
 */
function ehwow_cursusTable() {
 /**
	* - eerste cel met maandnaam/jaar moet rowspan hebben van het aantal geplande dagen in die maand
	*/
	$datums = wow_cursusdata::getCursusDatums( true );
	$usedheaders = array();
	$i = 0;
	$headers = array("Maand","Dag");
 foreach($datums as $d) {
	$m = substr($d->datum,5,2);
	$months[$m]++;
	//headers samenstellen, unieke cursusnamen
	if(!in_array($d->cursus,$usedheaders) ) {
	 $headers[] = $d->cursus;
	 $cols[$i] = $d->cursus;
	 $i++;
	}
	$usedheaders[] = $d->cursus;
 }

 $table = '<div>';
 $table .= '<table id="cursustabel">';
 $table .= '<colgroup>';
 foreach($headers as $h) {
	$table .= '<col class="ehcursuscol" id="'.strtolower(str_replace(" ","_",$h)).'"/>';
 }
 $table .='</colgroup>';
 $table .= '
	<thead>
	 <tr><th>'.implode("</th><th>",$headers).'</th></tr>
	</thead>
	<tbody>
 ';
 $monthsset = array();
 foreach( $datums as $d ) {
	$curmonth    = substr($d->datum,5,2);
	$curmonthstr = date_i18n("F Y",strtotime($d->datum));
	$curday      = date_i18n("j",strtotime($d->datum));
	$table .= '<tr>';
	//maand
	if(!in_array( $curmonth,$monthsset) ) $table .= '<td rowspan="'.$months[$curmonth].'">'.$curmonthstr.'</td>';
	//dag
	$table .= '<td>'.$curday.'</td>';

	//cursussen
	foreach($cols as $col) {
	 $cursus = ehwow_getCursusdataByDate($d->datum,$col);
	 if($cursus->naam != "") {
		$table .= '<td>'.$cursus->naam.'</td>';
	 } else {
		$table .= '<td style="background-color:#fff;">&nbsp;</td>';
	 }
	}
	$table .= '</tr>';
	$monthsset[] = $curmonth;
 }
 $table .= '
	</tbody>
 </table>
 ';
 $table .= '</div>';
 return $table;
}

/**
 * parse contentdata tbv filter
 *
 * @param string $content
 * @return string
 */
function ehwow_parsePage( $content ) {
 /**
	* @todo WP shortcode_atts toevoegen (eventueel)
	* shortcode_atts: http://codex.wordpress.org/Function_Reference/shortcode_atts
	*/
 $parsedcontent = preg_replace_callback("/\[(CURSUSTABEL)\]/i","ehwow_rewriteText",$content);
 //$parsedcontent = preg_replace_callback("/\[(CURSUSSTART)\]([a-z])\[\/CURSUSSTART\]/i","ehwow_rewriteText",$content);
 //$replacethis = array("&","&amp;nbsp;","&amp;euro;","<80>","-","&amp;dash;");
 //$replaceby   = array("&amp;","&nbsp;","&euro;","&euro;","-","&dash;");
 //return str_replace($replacethis,$replaceby,$parsedcontent);
 return $parsedcontent;
};

/**
 * geef data terug obv een array van matches
 *
 * @param array $match
 * @return string
 */
function ehwow_rewriteText($match){
 if(!is_array($match)) return false;
 $type = $match[1];
 $var  = $match[2];
 switch ( strtoupper($type) ) {
	case "CURSUSTABEL":
	 return ehwow_cursusTable($var);
	 break;
 }
}


