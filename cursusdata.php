<?php
/*
Plugin Name: EHBO Bernardus Cursusdata
Plugin URI: http://www.wipeout.nu/wordpress
Description: Plugin, ontwikkeld voor EHBO Bernardus in Delft, het is hiermee mogelijk om de cursussen en cursusdata te beheren
Version: 0.1
Author: WipeOut! webservices
Author URI: http://www.wipeout.nu
License: GPL2
*/

	global $ehwow_db_version;
	$ehwow_db_version = "1.0";

	require_once(plugin_dir_path( __FILE__ ).'/admin.php');
	require_once(plugin_dir_path( __FILE__ ).'/site.php');

/**
 * voeg een menuoptie toe aan admin menu
 * voeg javascript en callbackscripts toe
 * voeg stylesheets toe
 */
	add_action('admin_menu', 'ehwow_plugin_menu');
	add_action('admin_footer', 'my_action_javascript');
	add_action('wp_ajax_my_action', 'my_action_callback');
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery.ui.theme', plugin_dir_url( __FILE__ ) . 'smoothness/jquery-ui-1.8.16.custom.css');
	wp_enqueue_style('wow_cursusdata', plugin_dir_url( __FILE__ ) . 'wow_cursusdata.css');

	/**
	 * voeg contentfilter toe om de tabel (en eventueel andere content) in te voegen
	 */
	add_filter('the_content','ehwow_parsePage');

	/**
	 * initieer database
	 *
	 */
	register_activation_hook(__FILE__,'ehwow_install');

	function ehwow_plugin_menu() {
		$icon = plugin_dir_url( __FILE__ ).'redcross.png';
//  	add_options_page('WipeOut tables Options', 'WipeOut tables', 'manage_options', 'wowtableplugin', 'ehwow_plugin_options');
//  	add_plugins_page('Bernardus cursusdata', 'Bernardus Cursusdata', 'manage_options', 'wowcursusplugin', 'ehwow_plugin_options');
		add_menu_page('Bernardus cursusdata', 'Cursusdata', 'edit_pages','wowcursusplugin',  'ehwow_plugin_options', $icon,3);
 //add_menu_page( $page_title,             $menu_title, $capability,  $menu_slug,        $function,            $icon_url, $position )
	}



	/**
	 * Database setup; maak de tabellen aan
	 */
	function ehwow_install() {
		global $wpdb;
		global $ehwow_db_version;

		$table_name  = $wpdb->prefix . "ehwow_cursusdata";
		$table2_name = $wpdb->prefix . "ehwow_cursussen";

		$sql = "
			CREATE TABLE $table_name (
				datumid mediumint(9) NOT NULL AUTO_INCREMENT,
				datum datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				cursusid mediumint(9) NOT NULL,
				naam VARCHAR(55) NOT NULL,
				tekst text DEFAULT '',
				aangemaakt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid mediumint(9) NOT NULL,
				PRIMARY KEY (datumid)
			);
		";
		$sql2 = "
			CREATE TABLE $table2_name (
				cursusid mediumint(9) NOT NULL AUTO_INCREMENT,
				aangemaakt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid mediumint(9) NOT NULL,
				naam VARCHAR(55) DEFAULT '' NOT NULL,
				PRIMARY KEY (cursusid),
				UNIQUE KEY `naam` (`naam`)
			);
		";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		dbDelta($sql2);

		add_option("ehwow_db_version", $ehwow_db_version);
	}

	function ehwow_getCursusdataByDate( $datum,$cursus="" ) {
		global $wpdb;
		$wpdb->show_errors();
		$table_name  = $wpdb->prefix . "ehwow_cursusdata";
		if($cursus != "") {
			if(!is_numeric( $cursus )) {
				$w = " AND c.naam='{$cursus}'";
			} else {
				$w = " AND cursusid={$cursus}";
			}
			$datums = $wpdb->get_row( "SELECT cd.*,c.naam AS cursus FROM {$table_name} AS cd LEFT JOIN wp_ehwow_cursussen AS c USING(cursusid) WHERE datum='{$datum}'{$w}" );
		} else {
			$datums = $wpdb->get_results( "SELECT cd.*,c.naam AS cursus FROM {$table_name} AS cd LEFT JOIN wp_ehwow_cursussen AS c USING(cursusid) WHERE datum='{$datum}'" );
		}
		//error_log( print_r($datums,true) );
		return $datums;
	}

	/**
	 * Voeg een datum toe
	 * datum,cursusid,naam,tekst,aangemaakt,userid
	 *
	 * @param array $data datum,cursusid,naam,tekst,aangemaakt,userid
	 * @return boolean
	 */
	function ehwow_addDatum( $data ) {
		if( !is_array( $data ) ) return false;
		global $wpdb;
		$wpdb->show_errors();
		$current_user = wp_get_current_user();
		//$data['aangemaakt'] = "NOW()";
		$data['userid'] = $current_user->ID;
		print_r($data);
		$table_name  = $wpdb->prefix . "ehwow_cursusdata";
		$wpdb->insert( $table_name, $data );
		return ehwow_getDatum( $data['datum'],false );
	}

	/**
	 * voeg een cursus toe
	 *
	 * @param array $data
	 * @return array
	 */
	function ehwow_addCursus( $data ) {
		if( !is_array( $data ) || $data['naam'] == "" ) return false;
		global $wpdb;
//    $wpdb->show_errors();
		$current_user = wp_get_current_user();
		$data['userid'] = $current_user->ID;
		$table_name  = $wpdb->prefix . "ehwow_cursussen";
		$result = $wpdb->insert( $table_name, $data );
		return ehwow_getCursus($data['naam']);
	}

	function ehwow_cursusPlanned( $cursus ) {
		global $wpdb;
		$table_name  = $wpdb->prefix . "ehwow_cursusdata";
		if(is_numeric($cursus)) $w = "cursusid={$cursus}";
		else $w = "naam='{$cursus}'";
		$sql = "SELECT COUNT(*) AS n FROM {$table_name} WHERE {$w}";
		$res = $wpdb->get_row($sql, ARRAY_A);
		return $res['n'];
	}

	/**
	 * Verwijder een cursus
	 *
	 * @param mixed $cursus
	 */
	function ehwow_deleteCursus( $cursus ) {
		global $wpdb;
		//$wpdb->show_errors();
		$table_name  = $wpdb->prefix . "ehwow_cursussen";
		if(is_numeric($cursus))
			$w = "cursusid={$cursus}";
		else
			$w = "naam='{$cursus}'";
		$wpdb->query("DELETE FROM {$table_name} WHERE {$w}");
	}

	function ehwow_deleteDatum( $datum ) {
		global $wpdb;
		//$wpdb->show_errors();
		$table_name  = $wpdb->prefix . "ehwow_cursusdata";
		if(is_numeric( $datum ))
			$w = "datumid={$datum}";
		else
			return false;;
		$wpdb->query("DELETE FROM {$table_name} WHERE {$w}");
	}

	/**
	 * haal gegevens van een cursus op
	 *
	 * @param mixed $cursus cursusnaam of cursusid
	 * @return array
	 */
	function ehwow_getCursus( $cursus ) {
		global $wpdb;
		$table_name  = $wpdb->prefix . "ehwow_cursussen";
		if(is_numeric($cursus)) $w = "cursusid={$cursus}";
		else $w = "naam='{$cursus}'";
		$sql = "SELECT * FROM {$table_name} WHERE {$w}";
		$res = $wpdb->get_row($sql, ARRAY_A);
		return $res;
	};

	/**
	 * haal gegevens van een datum op
	 *
	 * @param mixed $cursus cursusnaam of cursusid
	 * @return array
	 */
	function ehwow_getDatum( $datum, $array = true ) {
		global $wpdb;
		$table_name  = $wpdb->prefix . "ehwow_cursusdata";
		if(is_numeric($datum)) $w = "datumid={$datum}";
		elseif( strlen($datum) >= 10 ) $w = "datum='{$datum}'";
		else return array();
		$sql = "SELECT cd.*,c.naam AS cursus FROM {$table_name} AS cd LEFT JOIN wp_ehwow_cursussen AS c USING(cursusid) WHERE {$w}";
		if($array)
			$res = $wpdb->get_row($sql, ARRAY_A);
		else
			$res = $wpdb->get_row($sql);
		return $res;
	};



class wow_cursusdata
{

	/**
	 * Haal alle cursusdatums op
	 *
	 * @param boolean $grouped set to true to group by datum
	 * @return array
	 */
	public static function getCursusDatums( $grouped=false ) {
		$table_name     =  $GLOBALS['wpdb']->prefix . "ehwow_cursusdata";
		if($grouped) $g = " GROUP BY datum";
		$datums =  $GLOBALS['wpdb']->get_results( "SELECT cd.*,c.naam AS cursus FROM {$table_name} AS cd LEFT JOIN wp_ehwow_cursussen AS c USING(cursusid){$g} ORDER BY datum" );
		return $datums;
	}

	/**
	 * haal cursussen op
	 * @param string $cursusid
	 * @return mixed
	 */
	public static function get_Cursussen( $cursusid = "" )
	{
		$w = "";
		if( strlen($cursusid) )
		{
			$w = " WHERE cursusid IN({$cursusid}) ";
		}
		$table_name = $GLOBALS['wpdb']->prefix . "ehwow_cursussen";
		$cursussen  = $GLOBALS['wpdb']->get_results( "SELECT * FROM {$table_name}{$w} ORDER BY naam" );
		return $cursussen;
	}
}
