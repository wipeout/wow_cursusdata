<?php
	function ehwow_plugin_options() {
		if (!current_user_can('manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}
		$cursussen = wow_cursusdata::get_Cursussen();
	?>
		<div class="wrap">
			<h2>Cursussen</h2>
			<table id="cursussen" class="wp-list-table widefat fixed posts">
				<thead>
					<tr><th><label for="c_cursusnaam">Cursusnaam</label></th><th>Aanmaakdatum</th><th>Door:</th></tr>
				</thead>
				<tfoot>
					<tr>
						<td><input type="text" name="c_cursusnaam" id="c_cursusnaam"/></td>
						<td colspan="2"><input type="button" name="c_submit" id="c_submit" value="Cursus toevoegen" class="button-secondary action" disabled="disabled"/></td>
					</tr>
					<tr><th>Cursusnaam</th><th>Aanmaakdatum</th><th>Door:</th></tr>
				</tfoot>
				<tbody>
				<?php
					foreach($cursussen as $c) {
						$userdata = get_userdata($c->userid);
						echo '
						<tr id="cursus_'.$c->cursusid.'">
							<td>'.$c->naam.' <span class="row-actions delete"> <a href="#" class="submitdelete">Verwijderen</a></span></td>
							<td>'.date_i18n('j F Y H:i' ,strtotime( $c->aangemaakt )).'</td>
							<td>'.$userdata->display_name.'</td>
						</tr>';
					}
				?>
				</tbody>
			</table>

		<h2>Cursusdata</h2>
			<table id="cursusdata" class="wp-list-table widefat fixed posts">
				<thead>
					<tr><th>Datum</th><th>Cursusnaam</th><th>Tekst</th><th>Aanmaakdatum</th><th>Door:</th></tr>
				</thead>
				<tfoot>
					<tr>
						<td><input type="text" name="cd_cursusdatum" id="cd_cursusdatum" class="mydatepicker"/></td>
						<td>
							<select type="text" name="cd_cursusid" id="cd_cursusid">
								<option value="">- Kies een cursus -</option>
							</select>
						</td>
						<td><input type="text" name="cd_datumnaam" id="cd_datumnaam"/></td>
						<td colspan="2"><input type="button" name="cd_submit" id="cd_submit" value="Datum toevoegen" class="button-secondary action" disabled="disabled"/></td>
					<tr><th>Datum</th><th>Cursusnaam</th><th>Tekst</th><th>Aanmaakdatum</th><th>Door:</th></tr>
				</tfoot>
				<tbody>
	<?php
		$cursusdata = wow_cursusdata::getCursusDatums();
		foreach($cursusdata as $c) {
			$userdata = get_userdata($c->userid);
			echo '
			<tr id="datum_'.$c->datumid.'">
				<td>'.date_i18n("D j F Y" ,strtotime( $c->datum )).' <span class="row-actions delete"> <a href="#" class="submitdelete">Verwijderen</a></span></td>
				<td>'.$c->cursus.'</td>
				<td>'.$c->naam.'</td>
				<td>'.date_i18n('j F Y H:i' ,strtotime( $c->aangemaakt )).'</td>
				<td>'.$userdata->display_name.'</td>
			</tr>';
		}
		echo '
				</tbody>
			</table>
		';
		echo '</div>';
	}

	function my_action_javascript() {
?>
<script>
jQuery(document).ready(function($) {
	function checkCursusform() {
		if( jQuery('#c_cursusnaam').length > 0 ) {
			var curname = jQuery('#c_cursusnaam').val();
			if( curname.length >= 2) {
				jQuery('#c_submit').removeAttr("disabled");
			} else {
				jQuery('#c_submit').attr("disabled", "disabled");
			}
		}
	}
	function checkCursusdataForm() {
		var curid = jQuery('#cd_cursusid').val();
		var curdat  = jQuery('#cd_cursusdatum').val();
		var curnaam = jQuery('#cd_datumnaam').val();
		//console.log(curname +' '+curdat);
		if( curid > 0 && curdat.length > 2 && curdat.length > 2 ) {
			jQuery('#cd_submit').removeAttr("disabled");
		} else {
			jQuery('#cd_submit').attr("disabled", "disabled");
		}
	}
	function buildDropdown() {
		jQuery('#cd_cursusid').empty();
		var data = { 'action':'my_action','do':'cursusDropdown' };
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('#cd_cursusid').append(response);
			jQuery('#c_submit').attr("disabled", "disabled");
			jQuery('#cd_submit').attr("disabled", "disabled");
		});
	};
	//init
	checkCursusform();
	checkCursusdataForm();
	buildDropdown();

	//check of de toevoegknoppen actief mogen zijn
	jQuery('#c_cursusnaam').keyup(checkCursusform);
	jQuery('#cd_cursusdatum').change(checkCursusdataForm);
	jQuery('#cd_cursusid').change(checkCursusdataForm);
	//jQuery('#cd_cursusid').live('change',checkCursusdataForm);
	jQuery('#cd_datumnaam').keyup(checkCursusdataForm);

	//datepicker instellen
	jQuery('.mydatepickerlll').datepicker("option", "dateFormat", 'yy-mm-dd' );
	jQuery('.mydatepicker').datepicker({
		buttonImage: "/wp-content/plugins/cursusdata/calendar_icon.gif",
		buttonImageOnly: true,
		dateFormat : 'yy-mm-dd',
		dayNamesMin: ['zo','ma','di','wo','do','vr','za'],
		monthNames:     ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december']
	});

	//cursus toevoegen
	jQuery('#c_submit').click(function(response){
		var naam = jQuery('#c_cursusnaam').val();
		var data = { 'action':'my_action','do':'addCursus','naam':naam };
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('#cursussen tr:last').after(response);
			//nieuwe cursus toevoegen aan de dropdown ...
			buildDropdown();
			//inputveld leegmaken
			jQuery('#c_cursusnaam').val();
		});
	});

	//datum toevoegen
	jQuery('#cd_submit').click(function(response){
		var datum    = jQuery('#cd_cursusdatum').val();
		var cursusid = jQuery('#cd_cursusid').val();
		var naam     = jQuery('#cd_datumnaam').val();
		var data = { 'action':'my_action','do':'addDatum','datum':datum,'cursusid':cursusid,'naam':naam };
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('#cursusdata tr:last').after(response);
			//form resetten
			buildDropdown();
			jQuery('#cd_datumnaam').val('');
			jQuery('#cd_cursusdatum').val('');
			jQuery('#cd_submit').attr('disabled','disabled');
		});
	});

	//cursus verwijderen
	jQuery('.submitdelete').click(function(response){
		response.preventDefault();
		var id  = $(this).closest('tr').attr('id')
		var data = { 'action':'my_action','do':'deleteCursus','id':id };
		jQuery.post(ajaxurl, data, function(response) {
			if( response == '' ) {
				jQuery('#'+ id).fadeOut();
				//cursus verwijderen uit de dropdown ...
				buildDropdown();
			} else {
				alert(response);
			}
		});
	});

});
</script>
<?php
	}

	function my_action_callback() {
		global $wpdb;
		$action = $_POST['do'];
		switch ($action) {
			case "addCursus":
				$naam = $_POST['naam'];
				$c = ehwow_addCursus( array('naam'=>$naam) );
			$userdata = get_userdata($c['userid']);
			echo '
			<tr id="cursus_'.$c['cursusid'].'">
				<td>'.$c['naam'].'</td>
				<td>'.date_i18n('j F Y H:i' ,strtotime( $c['aangemaakt'] )).'</td>
				<td>'.$userdata->display_name.'</td>
			</tr>';
				break;
			case "addDatum":
				$data['cursusid'] = $_POST['cursusid'];
				$data['datum']    = $_POST['datum'];
				$data['naam']     = $_POST['naam'];
				$data['tekst']    = $_POST['tekst'];
				$c = ehwow_addDatum( $data );
				$userdata = get_userdata($c->userid);
				echo '
				<tr id="datum_'.$c->datumid.'">
					<td>'.date_i18n("D j F Y" ,strtotime( $c->datum )).' <span class="row-actions delete"> <a href="#" class="submitdelete">Verwijderen</a></span></td>
					<td>'.$c->cursus.'</td>
					<td>'.$c->naam.'</td>
					<td>'.date_i18n('j F Y H:i' ,strtotime( $c->aangemaakt )).'</td>
					<td>'.$userdata->display_name.'</td>
				</tr>';
				break;
			case "cursusDropdown":
				$cursussen = wow_cursusdata::get_Cursussen();
				echo '<option value="">- Kies een cursus -</option>';
				foreach( $cursussen as $c ) {
						echo '<option value="'.$c->cursusid.'">'.$c->naam.'</option>';
				}
				break;
			case "deleteCursus":
				$id = $_POST['id'];
				list($type,$id) = explode("_",$id);
				if($type == "cursus") {
					//check of de cursus in een cursusdatum voorkomt
					$planned = ehwow_cursusPlanned( $id );
					if($planned >= 1) {
						echo "Cursus niet verwijderd; Deze cursus komt nog {$planned} maal in de planning voor!";
					} else {
					//verwijder de cursus
						ehwow_deleteCursus( $id );
					}
				} elseif ($type == "datum") {
					//verwijder de datum
					ehwow_deleteDatum( $id );
				}
				break;
		}
		exit();
	}
